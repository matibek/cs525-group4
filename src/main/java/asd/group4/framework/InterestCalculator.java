package asd.group4.framework;

public interface InterestCalculator {
    double calculateInterest(double balance);
}
