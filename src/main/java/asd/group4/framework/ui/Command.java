package asd.group4.framework.ui;

public interface Command {
    void execute(UIControl control);
}
