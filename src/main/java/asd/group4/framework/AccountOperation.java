package asd.group4.framework;

public enum AccountOperation {
    CREATED,
    DEPOSITED,
    WITHDREW,
    INTEREST,
    REPORT,
}
