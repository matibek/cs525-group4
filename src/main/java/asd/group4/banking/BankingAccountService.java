package asd.group4.banking;

import asd.group4.banking.interestCalculators.CompanyCheckingInterestCalculator;
import asd.group4.banking.interestCalculators.CompanySavingInterestCalculator;
import asd.group4.banking.interestCalculators.PersonalCheckingInterestCalculator;
import asd.group4.banking.interestCalculators.PersonalSavingInterestCalculator;
import asd.group4.banking.observer.EmailSender;
import asd.group4.framework.Account;
import asd.group4.framework.AccountService;
import asd.group4.framework.Customer;
import asd.group4.framework.Personal;

public class BankingAccountService extends AccountService {
    private static volatile BankingAccountService instance;

    private BankingAccountService() {
        super(BankingAccountDAO.getInstance());
        this.registerObserver(new EmailSender(this));
    }

    @Override
    public Account initAccount(String accountType, Customer customer) {
        if (customer instanceof Personal) {
            if (AccountTypes.valueOf(accountType) == AccountTypes.Checking) {
                return new CheckingAccount(new PersonalCheckingInterestCalculator());
            }
            return new SavingAccount(new PersonalSavingInterestCalculator());
        }
        if (AccountTypes.valueOf(accountType) == AccountTypes.Checking) {
            return new CheckingAccount(new CompanyCheckingInterestCalculator());
        }
        return new SavingAccount(new CompanySavingInterestCalculator());
    }

    public static BankingAccountService getInstance() {
        if (instance == null) {
            synchronized (BankingAccountService.class) {
                if (instance == null) {
                    instance = new BankingAccountService();
                }
            }
        }
        return instance;
    }
}
