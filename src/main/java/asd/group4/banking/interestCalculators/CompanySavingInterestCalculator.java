package asd.group4.banking.interestCalculators;

import asd.group4.framework.InterestCalculator;

public class CompanySavingInterestCalculator implements InterestCalculator {
    @Override
    public double calculateInterest(double balance) {
        return balance * 0.07;
    }
}
