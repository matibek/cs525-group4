package asd.group4.banking;

import asd.group4.framework.Account;
import asd.group4.framework.InterestCalculator;

public class CheckingAccount extends Account {
    public CheckingAccount(InterestCalculator interestCalculator) {
        super(interestCalculator);
    }

    @Override
    public String getAccountType() {
        return AccountTypes.Checking.name();
    }
}
