package asd.group4.banking;

import asd.group4.framework.Account;
import asd.group4.framework.InterestCalculator;

public class SavingAccount extends Account {
    public SavingAccount(InterestCalculator interestCalculator) {
        super(interestCalculator);
    }

    @Override
    public String getAccountType() {
        return AccountTypes.Saving.name();
    }
}
