package asd.group4.banking.commands;

import asd.group4.banking.BankingAccountService;
import asd.group4.framework.ui.UIControl;
import asd.group4.framework.ui.Command;

public class AddCompanyAccountCommand implements Command {
    public void execute(UIControl control) {
        BankingAccountService.getInstance().createAccount(control.getAccountNumber(), control.getCustomer(), control.getAccountType());
    }
}
