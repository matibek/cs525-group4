package asd.group4.banking.commands;

import asd.group4.banking.BankingAccountService;
import asd.group4.framework.ui.UIControl;
import asd.group4.framework.ui.Command;

public class WithdrawCommand implements Command {
    @Override
    public void execute(UIControl control) {
        BankingAccountService.getInstance().withdraw(control.getAccountNumber(), Double.parseDouble(control.getAmount()));
    }
}
