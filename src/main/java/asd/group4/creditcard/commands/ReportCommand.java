package asd.group4.creditcard.commands;

import asd.group4.creditcard.CreditAccountService;
import asd.group4.framework.ui.Command;
import asd.group4.framework.ui.UIControl;

public class ReportCommand implements Command {
    @Override
    public void execute(UIControl control) {
        CreditAccountService.getInstance().buildReport();
    }
}
