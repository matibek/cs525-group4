package asd.group4.creditcard.commands;

import asd.group4.creditcard.CreditAccountService;
import asd.group4.framework.ui.UIControl;
import asd.group4.framework.ui.Command;

public class AddPersonalAccountCommand implements Command {
    @Override
    public void execute(UIControl control) {
        CreditAccountService.getInstance().createAccount(control.getAccountNumber(), control.getCustomer(), control.getAccountType());
    }
}
