# A framework implementation for Banking system

The application uses several design patterns to create a framework for banking system.

It is built using pure java with Swing UI.

## System Design
![System diagram](./complete_uml.png "UML")

## Design patterns used in the project

### Strategy Pattern: 
Use when we want to calculate interest for each type of account (Personal Checking/Saving, Company Checking/Saving). Use InterestCalculator strategy.

Use when we want to calculate due payment of each type credit card account (Bronze, Silver, Gold). Use PaymentCalculator strategy.

### Singleton Pattern: 
Create only one instance of Main Form when each app want to use.

Create only one instance BankingAccountService, CreditAccountService. These look like service in the project should only need to instantiate one time.

### Factory Method Pattern
To initialize account type when creating account. Using it mixing with strategy account to create account type for Personal Saving/Checking account, Company Saving/Checking account, Credit card Bronze/Silver/Gold.
 
### Observer Pattern
Hook the Email sender to send email when there are change of balance in account.
Hook to Main Form to listen to the balance changing of account to update the grid on the UI.

### Template method pattern
Use in FormTemplate to generate form with many settings, defer some step to the subclass to initialize the settings like tables, button, panel, sizes, scroll panel

### Command Pattern
Each command in the UI is a command and will execute the service: AddNewPersonalCommand, AddNewCompanyCommand, DepositCommand, WithdrawCommand, AddIntersestCommand, ReportCommand

Main Form is the Invoker, the AccounService is the receiver to implement the action of the execute command.
